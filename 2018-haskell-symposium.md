# [Proceedings of Haskell Symposium 2018](https://icfp18.sigplan.org/track/haskellsymp-2018-papers#event-overview)

- [A Promise Checked Is a Promise Kept: Inspection Testing - *Joachim Breitner*](https://arxiv.org/pdf/1803.07130.pdf)
    <details>
        <summary>My note</summary>
        <a href="https://github.com/nomeata/inspection-testing">The introduced plugin is here.</a><br />
        Seems that some of the features implemented thanks to the typed Core.
    </details>
- [AutoBench: Comparing the Time Performance of Haskell Programs - *Martin A. T. Handley, Graham Hutton*](http://www.cs.nott.ac.uk/~pszgmh/autobench.pdf)
    - [Firegures are also available](http://www.cs.nott.ac.uk/~pszgmh/autobench-graphs.pdf).
    <details>
        <summary>My note</summary>
        <a href="https://github.com/mathandley/AutoBench">The introduced package is here.</a><br />
        Check the details of the executable: How to generate input for the programs to test? Can it generate any Arbitrary instances?<br />
        Can it save the randomly generated data to for reproducibility?
    </details>
- [Branching Processes for QuickCheck Generators - *Agustín Mista, Alejandro Russo, John Hughes*](https://arxiv.org/pdf/1808.01520.pdf)
    <details>
        <summary>My note</summary>
        Too hard to understand the details for me... Seems to require more knowledge of math.<br />
        But in short, it says they invented a cool way to define instances of Arbitrary.
    </details>
- [Coherent Explicit Dictionary Application for Haskell - *Thomas Winant, Dominique Devriese*](https://people.cs.kuleuven.be/~thomas.winant/coherent-explicit-dictionary-application-for-haskell.pdf)
    - [Full formalisation and coherence proof](https://arxiv.org/pdf/1807.11267.pdf)
- [Deriving Via: or, How to Turn Hand-Written Instances into an Anti-pattern - *Baldur Blöndal, Andres Löh, Ryan Scott*](https://www.kosmikus.org/DerivingVia/deriving-via-paper.pdf)
- [Ghosts of Departed Proofs (Functional Pearl) - *Matt Noonan*](https://github.com/matt-noonan/gdp-paper/releases)
- [Improving Typeclass Relations by Being Open - *Guido Martínez, Mauro Jaskelioff, Guido De Luca*](https://www.fceia.unr.edu.ar/~mauro/pubs/cm-conf.pdf)
- [Rhine: FRP with Type-Level Clocks - *Manuel Bärenz, Ivan Perez*](https://www.manuelbaerenz.de/sites/default/files/Rhine_0.pdf)
- [Suggesting Valid Hole Fits for Typed-Holes (Experience Report) - *Matthías Páll Gissurarson*](https://mpg.is/papers/gissurarson2018suggesting-xp.pdf)
- [Theorem Proving for All: Equational Reasoning in Liquid Haskell (Functional Pearl) - *Niki Vazou, Joachim Breitner, Rose Kunkel, David Van Horn, Graham Hutton*](https://arxiv.org/pdf/1806.03541.pdf)
- [Type Variables in Patterns - *Richard A. Eisenberg, Joachim Breitner, Simon Peyton Jones*](https://arxiv.org/pdf/1806.03476.pdf)

## PDF Not freely available (yet)

- A High-Performance Multicore IO Manager Based on libuv (Experience Report) - *Dong Han, Tao He*
    - A related, older paper is here: <http://haskell.cs.yale.edu/wp-content/uploads/2013/08/hask035-voellmy.pdf>
- Autobahn 2.0: Minimizing Bangs while Maintaining Performance (System Demonstration) - *Marilyn Sun, Kathleen Fisher*
- Embedding Invertible Languages with Binders: A Case of the FliPpr Language - *Kazutaka Matsuda, Meng Wang*
- Generic Programming of All Kinds - *Alejandro Serrano, Victor Cacciari Miraldo*
- The Thoralf Plugin: For Your Fancy Type Needs - *Divesh Otwani, Richard A. Eisenberg*
    - [The README of the plugin](https://github.com/Divesh-Otwani/the-thoralf-plugin#usage) says "Read our haskell symposium submission! Link forthcoming."
